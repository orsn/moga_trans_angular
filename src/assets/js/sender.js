const sgMail = require('@sendgrid/mail');
const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const app = express();
const https = require('https');
const fs = require('fs');
require('dotenv').config();
const key = process.env.SENDGRID_API_KEY;
const PORT = process.env.PORT;

sgMail.setApiKey(key);
const bagkey = fs.readFileSync('/etc/letsencrypt/live/bagazowkachojnice.pl/privkey.pem');
const bagcert = fs.readFileSync('/etc/letsencrypt/live/bagazowkachojnice.pl/cert.pem');

const options = {
  key: bagkey,
  cert: bagcert,
};
const server = https.createServer(options, app);

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/', function(req, res) {
  res.send("Hello from server");
})

app.post('/enroll', function(req, res) {
  // console.log(req.body);
  const msg_client = {
    to: `${req.body.email}`,
    from: `kontakt@bagazowkachojnice.pl`,
    subject: `Witaj ${req.body.imie}!, Dziękujemy za kontakt!`,
    html: `<strong>Witaj!</strong><br>
            Dziękujemy za kontakt!<br>Postaramy się odpowiedzieć najszybciej jak to będzie możliwe<br>
            Gdyby kontakt nie nastąpił w ciągu godziny to znaczy, żę pewnie jesteśmy w trasie<br>
            W takim wypadku Prosimy o telefon:<br><strong>+48 790 579 380</strong>`,
  };
  const msg_owner = {
    to: `kontakt@bagazowkachojnice.pl`,
    from: `${req.body.email}`,
    subject: `Nowa prośba kontaktu od: ${req.body.email}`,
    html: `<p>Masz nową prośbę kontaktu</p>
          <h3>Informacje:</h3>
          <ul>
            <li color: red><strong>Imię:</strong> ${req.body.imie}</li>
            <li><strong>Telefon:</strong> ${req.body.telefon}</li>
            <li><strong>Email:</strong> ${req.body.email}</li>
          </ul>
          <h3>Wiadomość od klienta:</h3>
          <p>${req.body.wiadomosc}</p>`,
  };

  sgMail.send(msg_owner);
  sgMail.send(msg_client);
  res.status(200).send({"message": "data recived"});
})

server.listen(PORT, function () {
  console.log('HTTP Express server is up!');
}


export class User {
    constructor(
        public imie: string,
        public email: string,
        public telefon: number,
        public wiadomosc: string,
        // public info: string,
    ) {}
}

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Injectable } from '@angular/core';
import { MessageService } from '../message.service';
import { User } from './user';
import { faCoffee } from '@fortawesome/free-solid-svg-icons';

@Injectable()
@Component({
  selector: 'app-kontakt',
  templateUrl: './kontakt.component.html',
  styleUrls: ['./kontakt.component.css']
})



export class KontaktComponent implements OnInit {
  submitted = false;
  submitted_msg = false;
  myForm: FormGroup;
  info: string[] = [
    'Gazeta', 'Reklama w sklepie', 'Internet', 'Od znajomego'
  ];
  email: '';
  wiadomosc: '';
  imie: '';
  telefon: number ;
  constructor(private fb: FormBuilder,
              private _MessageService: MessageService) { }

  userModel = new User(this.imie, this.email, this.telefon, this.wiadomosc);

  OnSubmit() {
    // console.log(this.myForm)
    this.submitted = true;
    this.submitted_msg = true;
    // console.log("this.userModel" + this.userModel.);
    this._MessageService.enroll(this.userModel)
    .subscribe(
      response => console.log('Success!', response),
      error => console.log('Error!', error)
    );

  }
  ngOnInit() {
    this.myForm = this.fb.group({
      email: ['', [
        Validators.email,
        Validators.required
      ]],
      wiadomosc: ['', [
        Validators.minLength(10),
        Validators.maxLength(500),
        Validators.required
      ]],
      // info: '',
      imie: ['', [
        Validators.maxLength(20),
      ]],
      telefon: ['', [
        Validators.min(9),
        Validators.required
      ]],
    });

}}

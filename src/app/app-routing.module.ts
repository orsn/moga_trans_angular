import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GlownaComponent } from './glowna/glowna.component';
import { InfoComponent } from './info/info.component';
import { KontaktComponent } from './kontakt/kontakt.component';


const routes: Routes = [
  // to mozemy odblokowac aby main strone przekierowac np odrazu na kontant
  { path:  '', redirectTo:  'kontakt', pathMatch:  'full' },
  {
  path: 'info',
  component: InfoComponent
  },
  {
  path: 'kontakt',
  component: KontaktComponent
  },
  {
    path: 'glowna',
    component: GlownaComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

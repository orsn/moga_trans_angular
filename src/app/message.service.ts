import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { User } from './kontakt/user';

@Injectable({
    providedIn: 'root'
})
export class MessageService {

  _url = 'https://bagazowkachojnice.pl:2967/enroll';
  constructor(private http: HttpClient) { }
// to make POST request
  enroll(user: User) {
      // console.log(user.imie);

      return this.http.post<any>(this._url, user);

  }
}

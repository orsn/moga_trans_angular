import { Component } from '@angular/core';

// JS function to slide burger manu
const Slider = () => {
  const burger = document.querySelector('.burger');
  const nav = document.querySelector('.on_click');
  const navLinks = document.querySelectorAll('.btn');

  burger.addEventListener('click', () => {
      nav.classList.toggle('nav-active');
      burger.classList.toggle('change');
  });

};

// declare var jQuery: any;
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'moga-trans-app';

  ngOnInit(){
    Slider()
  }
}
